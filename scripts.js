$(window).load(function() {
		$('#mc-embedded-subscribe').hide();
		$('#mc-embedded-subscribe').css("visibility", "visible");
		$('#notification_container').fadeOut(0);
		$("#mc_embed_signup").css({'width':($("#mc_embed_signup>p").width()+1+'px')}); //Set Notification container height so that the text box doesnt't scale//
	});

$(document).ready(function(){
	
// Submit button visibility //
	$('input[type=email]').each(function() {
		var elem = $(this);
		elem.data('oldVal', elem.val());
		elem.bind("propertychange keyup input paste", function(){
			if (elem.data('oldVal') !== elem.val()) {
				elem.data('oldVal', elem.val());
					$('#mc-embedded-subscribe').fadeIn(400);
				}
			else if (elem.data('oldVal') === ""){
				$('#mc-embedded-subscribe').fadeOut(400);
				}
			});
		});

//Textbox focus trigger//
	$('input').focusin(function() {
        $(this).parent().addClass('focus');
    });
	$('input').focusout(function() {
        $(this).parent().removeClass('focus');
    });
	
	
//Email Signup Form//
	var notificationTimer;	//Global timer variable required for clearTimeout()//
	$(function () {
		var $form = $('#mc-embedded-subscribe-form');
		$('#mc-embedded-subscribe').on('click', function(event) {
			if(event) {
				event.preventDefault();
				}
			$('#notification_container').clearQueue().fadeOut(200);
			$('#notification_container').html('<span></span>');
			register($form);
			$(this).fadeOut(250);
			$('#loadingAnimation').delay(250).fadeIn(100);
			});
		});
		
	function register($form) {
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize(),
			cache       : false,
			dataType    : 'json',
			contentType: "application/json; charset=utf-8",
			timeout: 10000,
			error       : function() {
				$('#notification_container').html('<span class="alert"><span class="iconFont">&#10060</span> Could not connect to server. Please try again later.</span>');
				$('#notification_container').fadeIn(500);
				notificationTimer = setTimeout(function() {
					if ($('#notification_container .alert').css('opacity') === '1') {
						$('#notification_container .alert').fadeToggle(2000);}
					}, 7000);
				$('#notification_container').hover(function(){ //Prevent notification from fading out during hover//
					clearTimeout(notificationTimer);
					$('#notification_container .alert').filter(':animated').stop(true, false).fadeToggle(200);
					},
					function(){
						notificationTimer = setTimeout(function() {
							if ($('#notification_container .alert').css('opacity') === '1') {
								$('#notification_container .alert').fadeToggle(2000);}
						}, 7000);
					});
				$('#mc-embedded-subscribe').filter(':not(:animated)').delay(500).fadeIn(250); //Bring back the submit button//
				},
					
			success     : function(data) {
				var message;
				if (data.result !== "success") {
					message = data.msg.substring(4);
					$('#notification_container').html('<span class="alert">'+'<span class="iconFont">&#10060</span> Error: '+message+'</span>');
					$('#formContainer').filter(':not(:animated)').effect('shake', {distance: 3});
					$('#mc-embedded-subscribe').delay(500).fadeIn(250);
					$('#notification_container').fadeIn(500); //Fade in the notification//
					notificationTimer = setTimeout(function() {
						if ($('#notification_container .alert').css('opacity') === '1') {
							$('#notification_container .alert').fadeToggle(2000);}
						}, 7000);
					$('#notification_container').hover(function(){ //Prevent notification from fading out during hover//
						clearTimeout(notificationTimer);
						$('#notification_container .alert').filter(':animated').stop(true, false).fadeToggle(200);
						/*if ($('#notification_container .alert').css('opacity') < 1) {	//Trigger fadeToggle only is animation is already fading//
							$('#notification_container .alert').stop(true, false).fadeToggle(200);
							}*/
						},
						function(){
							notificationTimer = setTimeout(function() {
								if ($('#notification_container .alert').css('opacity') === '1') {
									$('#notification_container .alert').fadeToggle(2000);}
								}, 7000);
						});
					}
				else {
					message = '<span class="iconFont">&#10003</span> Done! Check your mailbox and verify.';
					$('#notification_container').html('<span class="success">'+message+'</span>');
					clearTimeout(notificationTimer);
					$('#notification_container').fadeIn(500);
					$('#mce-EMAIL').blur();
					}
				},
			complete : function(){
				$('#loadingAnimation').fadeOut(500);
				}
			});
		}

	});